package com.example.fsoft.demo.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class Student {

    private Integer id;
    private String name;
    private int age;
    private String major;
    private boolean gender;
    private String studentCode;

    public Student (Student student) {
        this.setAge(student.getAge());
        this.setStudentCode(student.getStudentCode());
        this.setName(student.getName());
        this.setGender(student.isGender());
        this.setMajor(student.getMajor());
    }
}
