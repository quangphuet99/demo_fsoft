package com.example.fsoft.demo.Controller;

import com.example.fsoft.demo.Model.Student;
import com.example.fsoft.demo.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/api/students")
    public String getAllStudent(Model model, Principal principal) {
        List<Student> studentList = studentService.getAllStudent();
        model.addAttribute("studentList", studentList);

        return "/student/students";
    }

    @GetMapping("/api/student/{id}")
    public String getDetailStudent(Model model, @PathVariable int id) {
        Student student = studentService.getDetailStudent(id);
        model.addAttribute("student", student);

        return "/student/studentDetail";
    }

    @GetMapping("/api/student/update/{id}")
    public String getStudentToUpdate(Model model, @PathVariable Integer id) {
        Student student = new Student();
        if (id != -1) {
            student = studentService.getDetailStudent(id);
        }
        else {
            student.setId(-1);
        }
        model.addAttribute("student", student);

        return "/student/studentUpdate";
    }

    @DeleteMapping("/api/student/delete/{id}")
    public String deleteStudent(@PathVariable int id) throws Exception {
        Student student = studentService.getDetailStudent(id);
        if (student != null) {
            studentService.deleteStudent(id);
        }
        else {
            throw new Exception("Sinh viên không tồn tại.");
        }
        return "/student/students";
    }

    @PostMapping("/api/student/update")
    public String updateStudent(@ModelAttribute("student") Student student) {
        if (student.getId() == -1) {
            Student newStudent = new Student(student);
            studentService.addStudent(newStudent);
        }
        else {
            studentService.editStudent(student);
        }
        return "redirect:/api/students";
    }

}

